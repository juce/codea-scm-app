Diff = class()

function Diff:init()
    -- you can accept and set parameters here
    self.window = {20,20,WIDTH - 40,HEIGHT - 40}
    self.diffColors = {
        color(189, 189, 189, 255),
        color(192, 31, 18, 255),
        color(55, 166, 44, 255),
        color(81, 107, 206, 255)
    }
    
    self.markerX = WIDTH - 60
    self.markerY = HEIGHT - 60
    self.markerW = 35
    self.markerH = 35
    self.markerAlpha = 255
    
    self.mnw = {x = self.markerX, y = self.markerY + self.markerH}
    self.msw = {x = self.markerX, y = self.markerY}
    self.mne = {x = self.markerX + self.markerW, y = self.markerY + self.markerH}
    self.mse = {x = self.markerX + self.markerW, y = self.markerY}
end

function Diff:draw()
    -- Codea does not automatically call this method
    if self.visible then
        pushStyle()
        fill(35, 35, 39, 245)
        rect(10,10,WIDTH-20,HEIGHT-20)
        clip(unpack(self.window))
        
        -- text
        local chunks = self.chunks
        local colors = self.colors
        local sizes = self.sizes
        if chunks then
            local w, h
            textWrapWidth(WIDTH - 40)
            textAlign(LEFT)
            font("Inconsolata")
            fontSize(17)
            if not self.ypos then
                -- calculate initial position
                w, h = textSize(chunks[1])
                sizes[1] = h
                self.ypos = HEIGHT - 20 - h
                self.miny = self.ypos
                self.ylimit = 20
            end
            local y = self.ypos + sizes[1]
            local n = #chunks
            for j=1,n do
                local c = chunks[j]
                h = sizes[j]
                if not h then
                    w, h = textSize(c)
                    sizes[j] = h
                    self.ylimit = self.ylimit + h
                    if j == n and not self.maxy then
                        self.maxy = math.max(self.ylimit, self.miny)
                    end
                end
                y = y - h
                if y >= -HEIGHT and y <= HEIGHT then
                    fill(colors[j])
                    text(c, 20, y)
                end
            end
        end
        
        -- check for scroll ends
        if self.scrollTween then
            local ypos = self.ypos
            if ypos + 150 < self.miny then
                self:stopScroll()
                self:bounceBack()
            elseif self.maxy and self.maxy < ypos - 150 then
                self:stopScroll()
                self:bounceBack()
            end     
        end
        
        -- closer
        if self.markerAlpha > 0 then
            smooth()
            strokeWidth(6.0)
            stroke(59, 59, 59, self.markerAlpha)
            line(self.msw.x, self.msw.y, self.mne.x, self.mne.y)
            line(self.mnw.x, self.mnw.y, self.mse.x, self.mse.y)
        end
        popStyle()
    end
end

function Diff:setContent(t)
    -- break the text into chunks
    local linesInChunk = 20
    local chunks = {}
    local colors = {}
    local diffColors = self.diffColors
    local pos, len = 1, #t
    local s, lc = pos, 0
    local prevState, state = 1, 1
    while pos <= len do
        local bb,be = t:find("[\r]?\n", pos)
        if not be then
            be = len
        end
        lc = lc + 1
        local f = t:sub(pos,pos)
        if t:sub(pos,be):match("Binary files .* differ%s*$") then
            state = 4
        elseif f == "+" then 
            state = 3
        elseif f == "-" then
            state = 2
        else
            state = 1
        end
        if lc >= linesInChunk or be == len or prevState ~= state then
            if prevState == state then
                chunks[#chunks + 1] = t:sub(s, be)
                colors[#colors + 1] = diffColors[state]
                pos = be + 1
                s = pos
                lc = 0
            else
                chunks[#chunks + 1] = t:sub(s, pos - 1)
                colors[#colors + 1] = diffColors[prevState]
                s = pos
                lc = 0
            end
        else
            pos = be + 1
        end 
        prevState = state
    end
       
    self.chunks = chunks
    self.colors = colors
    self.sizes = {}
end 

function Diff:show(content)
    self:setContent(content)
    self.visible = true
    self:showMarker()
end

function Diff:hide()
    self.visible = nil
    self.miny, self.maxy = nil, nil
    self.ypos, self.ylimit = nil, nil
end

function Diff:showMarker()
    if self.markerTween then
        tween.stop(self.markerTween)
    end
    self.markerAlpha = 255
    tween.delay(1.0, function()
        self.markerTween = tween(1.0, self, {markerAlpha = 0}, tween.easing.linear,
            function() self.markerTween = nil end)
    end)        
end

function Diff:stopScroll()
    if self.scrollTween then
        tween.stop(self.scrollTween)
        self.scrollTween = nil
    end
end

function Diff:scrollFinish(touch)
    self:stopScroll()
    -- inertia
    local ypos = self.ypos
    if self.miny <= ypos and (not self.maxy or ypos <= self.maxy) then
        ypos = self.ypos + touch.deltaY*30
        self.scrollTween = tween(1.0, self, {ypos = ypos}, 
            tween.easing.quadOut, function()
                self:bounceBack()
            end)
    else
        self:bounceBack()
    end
end
        
function Diff:bounceBack()
    -- bounce back if needed
    local ypos = math.max(self.miny, self.ypos)
    ypos = self.maxy and math.min(self.maxy, ypos) or ypos
    tween(0.3, self, {ypos = ypos},
        tween.easing.quadOut, function()
        self.scrollTween = nil
    end)
end

function Diff:touched(touch)
    -- Codea does not automatically call this method
    if self.visible then
        if touch.state == BEGAN then
            self:stopScroll()
        elseif touch.state == ENDED then
            self.wasMoving, self.moving = self.moving, nil
            if self:insideMarker(touch) then
                self:hide()
                return true
            elseif not self.wasMoving then
                -- show closer marker
                self:showMarker()
            end
            -- smooth scrolling
            if self.wasMoving then
                self:scrollFinish(touch)
            end
        elseif touch.state == MOVING then
            self.moving = true
            self.ypos = self.ypos + touch.deltaY
        end
        return true
    end
end

function Diff:insideMarker(touch)
    return (self.msw.x <= touch.x and touch.x <= self.mne.x
        and self.msw.y <= touch.y and touch.y <= self.mne.y)
end
