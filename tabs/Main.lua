-- codea-scm

supportedOrientations(LANDSCAPE_ANY)

-- Use this function to perform your initial setup
function setup()
    displayMode(STANDARD)
    saveG()

    saveProjectInfo("Author", "Anton Jouline")
    saveProjectInfo("Description", "Source control hub for your Codea projects")
    
    -- globals
    serializer = Serializer().engine
    apiBase = readLocalData("apiBase") or "https://codea-scm.aws.mapote.com"   
    message = {}
        
    identity = readLocalData("identity")
    if identity then
        local pub
        user = string.match(identity, "([^:]+)")
        pub, private_key = readKeys(user)
    end
    
    setParameters()
    initProjects()
    
    if not identity then
        print("No identity. Please log in.")
    end
    
    VERSION = readLocalData("version") or "master"
    STATUS = user and string.format("%s @ %s", user, VERSION) or VERSION
    STATUS = STATUS .. "\n" .. apiBase
    sx,sy = nil,nil

    -- diff window   
    diffW = Diff()
end

function hexDecode(s)
    return s and string.gsub(s, "..", function(v)
        return string.char(tonumber(v, 16))
    end) or nil
end

function hexEncode(s)
    return s and string.gsub(s, ".", function(v)
        return string.format("%02x", string.byte(v))
    end) or nil
end

function readProjectFile(project, name, warn)
    local path = os.getenv("HOME") .. "/Documents/"
    local file = io.open(path .. project .. ".codea/" .. name,"r")
    if file then
        local plist = file:read("*all")
        file:close()
        return plist
    elseif warn then
        print("WARNING: unable to read " .. name)
    end
end

function readProjectPlist(project)
    return readProjectFile(project, "Info.plist", true)
end

function saveProjectFile(project, name, plist)
    local path = os.getenv("HOME") .. "/Documents/"
    local file = io.open(path .. project .. ".codea/" .. name, "w")
    if file then
        file:write(plist)
        file:close()
    else
        print("WARNING: unable to save " .. name)
    end
end

function saveProjectPlist(project, plist)
    return saveProjectFile(project, "Info.plist", plist)
end

function saveG()
    if not g then
        local g = {}
        for k,v in pairs(_G) do
            g[k] = v
        end
        _G.g = g
    end
end

-- this function mimics some of the behaviour of Codea's restart.
-- we don't really need a full restart, just some state reset.
function reset()
    tween.delay(0.2, function()
        -- clear global vars
        local g = g
        local G = _G
        for k,v in pairs(G) do
            G[k] = nil
        end
        for k,v in g.pairs(g) do
            G[k] = v
        end
        _G.g = g
        -- run setup
        output.clear()
        parameter.clear()
        setup()
    end)
end

function urlEncode(str)
    if str then
        str = string.gsub(str, "\n", "\r\n")
        str = string.gsub(str, "([^%w %-%_%.%~])",
            function (c) return string.format("%%%02X", string.byte(c)) end)
        str = string.gsub(str, " ", "+")
    end
    return str    
end

function readKeys(username)
    local pub = readGlobalData("codea-scm.key." .. username .. ".pub")
    local key = readGlobalData("codea-scm.key." .. username .. ".key")
    return pub, key
end

function saveKeys(username, pub, key)
    saveGlobalData("codea-scm.key." .. username .. ".pub", pub)
    saveGlobalData("codea-scm.key." .. username .. ".key", key)
end

function manageKeys(enteredName, backAction)
    if user then
        parameter.action("Show public key", function()
            local nick = tostring(user)
            local public_key = readKeys(nick)
            openURL(string.format("%s/whoami?nick=%s&public_key=%s",
                apiBase, urlEncode(nick), urlEncode(public_key or "")
            ), true)
        end)
    end
    if not user then
        parameter.action("New keypair", function()
            parameter.clear()
            local pub, pk = readKeys(enteredName)
            if pub and pk then
                message.warning = "This will overwrite your existing keypair. " ..
                    "To proceed, click 'make keys'"
                parameter.watch("message.warning")
            else
                message.information = "This will create a new pair of SSH keys and store " ..
                    "them on your device for user: " .. enteredName
                parameter.watch("message.information")
            end
            parameter.action("make keys", function()
                local name = enteredName
                if #name < 3 then
                    print("PROBLEM: username too short. Minimum - 3 chars")
                    setParameters()
                    return
                end
                print("Downloading new keypair ...")
                http.request(apiBase .. "/keymaker",
                    function(data, status, headers)
                        if tonumber(status) == 200 then
                            local t = serializer.decode(data)
                            saveKeys(name, t.public_key, t.private_key)
                            saveLocalData("identity", name .. ":nopass")
                            print(string.format("Keypair stored for: %s", name))
                            tween.delay(1.0, reset)
                        else
                            print("ERROR: " .. data)
                        end
                    end, function(err)
                        print("PROBLEM: " .. err)
                    end,
                    {method = "POST"})
                setParameters()
            end)
            parameter.action("cancel", function()
                (backAction or setParameters)()
            end)
        end)
    else
        parameter.action("Delete keypair", function()
            parameter.clear()
            message.warning = "This will permanently delete your keypair. " ..
                "You will then need to create a new one, in order to log " ..
                "back in as: " .. user
            parameter.watch("message.warning")
            parameter.action("Ok, delete it", function()
                saveKeys(user, nil, nil)
                saveLocalData("identity", nil)
                reset()       
            end)
            parameter.action("cancel", setParameters)
        end)
    end
end

function checkReturn(global, value, cb)
    if value:sub(#value,#value) == "\n" then
        showKeyboard()
        hideKeyboard()
        _G[global] = value:gsub("\n","")
        if cb then cb() end
    end
end

function loginMenu()
    parameter.clear()
    parameter.text("username", function(value) 
        checkReturn("username", value, doLogin)
    end)
    parameter.action("login", doLogin)
    parameter.action("cancel", setParameters)
end

function doLogin()
    if #username < 3 then
        print("PROBLEM: username too short. Minimum - 3 chars")
        return
    end
    local pub, pk = readKeys(username)
    if not pub or not pk then
        parameter.clear()
        message.greeting = "Hello, " .. username .. ". You have no keys stored. " ..
            "Press 'New keypair' to create them."
        parameter.watch("message.greeting")
        manageKeys(username)
        parameter.action("cancel", setParameters)
        return
    end                      
    saveLocalData("identity", username .. ":nopass")
    username, password = nil, nil
    reset()
end
  
function setParameters()
    parameter.clear()
    if readLocalData("identity") then
        parameter.action(string.format("User (%s)", user), function()
            parameter.clear()
            parameter.action("Logout", function()
                saveLocalData("identity",nil)
                reset()
            end)
            manageKeys(user)
            parameter.action("cancel", setParameters)
        end)
    else
        parameter.action("Login", loginMenu)
    end
    parameter.action("link project", function()
        parameter.clear()
        parameter.text("project",function()
            if project and project ~= "" then
                project = project:gsub("\n","")
                remote = readLocalData("project." .. project .. ".remote")
            end
        end)
        parameter.text("remote")
        parameter.action("link", function()
            if project and project ~= "" then
                project = project:gsub("\n","")
                saveLocalData("project." .. project .. ".remote", remote)
                initProjects()
            end
            setParameters()
        end)
        parameter.action("cancel", function() setParameters() end)
    end)
    parameter.action("unlink project", function()
        parameter.clear()
        parameter.text("project")
        parameter.action("unlink", function()
            if project and project ~= "" then
                project = project:gsub("\n","")
                saveLocalData("project." .. project .. ".remote", nil)
                initProjects()
            end
            setParameters()
        end)
        parameter.action("cancel", function() setParameters() end)
    end)
    parameter.boolean("checkOnLoad", readLocalData("checkOnLoad"), function(value)
        saveLocalData("checkOnLoad", value or nil)
    end)
    parameter.action("Help", function() 
        openURL("https://codea-scm.aws.mapote.com", true)
    end)
end
    
function initProjects()
    panel = Panel(12, 72, WIDTH-24, HEIGHT-84)
    
    -- clear global arrays
    buttons, statuses, commitButtons, resetButtons = {}, {}, {}, {}
        
    -- read project data from local storage
    local count = 0
    for i,p in iterProjects() do    
        local project = p.name
        print("Project: " .. p.name)
        local t = listProjectTabs(p.name)
        for _,k in ipairs(t) do
            local tab = readProjectTab(p.name .. ":" .. k)
            print("Tab: " .. k .. " (" .. #tab .. " chars)")
        end
        
        local b = Button(project, 80, HEIGHT-20-i*55, 300, 50)
        b.clicked = function(x) checkStatus(x, true, showDiff) end
        buttons[#buttons + 1] = b
        panel:add(b)
        
        local s = Status(b.x + 300 + 30, b.y + 25, 45, project)
        statuses[#statuses + 1] = s             
        b.status = s
        b.project = p
        panel:add(s, 45)
        
        local cb = Button("push", s.x + 50, b.y, 80, 50)
        cb.clicked = commitAndPushProject
        cb.status = s
        cb.project = p
        commitButtons[#commitButtons + 1] = cb
        panel:add(cb)
        
        local rb = Button("pull", cb.x + 190, b.y, 70, 50)
        rb.clicked = pullProject
        rb.status = s
        rb.project = p
        resetButtons[#resetButtons + 1] = rb
        panel:add(rb)
        
        if user then
            -- schedule a status check
            if checkOnLoad then
                tween.delay(1.0, function() checkStatus(b) end)
            end
        end
            
        count = count + 1
    end
    print("Num projects: " .. count)
end

function checkStatus(b, getDiff,f)
    if b.status.working then return end
    b.status:startAnimation()
    status(b.project, getDiff, function(...)
        local t = serializer.decode(...)
        b.status.c = ({
            [256] = Status.blue,
            [0] = Status.green,
        })[t.status] or Status.grey
        if f then f(t) end
        b.status:stopAnimation()
    end, function(err)
        print("PROBLEM: " .. tostring(err))
        b.status:stopAnimation()
        b.status.c = Status.grey
    end)
end

function commitAndPushProject(b)
    parameter.clear()
    parameter.text("comment")
    parameter.action("commit", function()
        setParameters()
        if b.status.working then return end
        b.status:startAnimation()
        push(b.project, comment, function(res)
            b.status:stopAnimation()
            local t = serializer.decode(res)
            if t.commit_status == 0 and t.push_status == 0 then
                b.status.c = Status.green
            end
        end, function(err)
            print("PROBLEM: " .. tostring(err))
            b.status:stopAnimation()
            b.status.c = Status.grey
        end)
        comment = nil
    end)
    parameter.action("cancel", function()
        setParameters()
    end)
end

function getLog(b)
    b.status:startAnimation()
    log(b.project, function(res)
        b.status:stopAnimation()
        local t = serializer.decode(res)
        pullProject(b, t.log, true)
    end)
end

function pullProject(b, logt, showHistory)
    parameter.clear()
    parameter.text("version","latest")
    if showHistory and logt then
        parameter.number("history", 1, #logt, 1, function(value)
            value = math.floor(value+0.5)
            history = value
            local tags = logt[value].tags
            if tags then
                tags = tags:gsub("origin/[^%s]+",""):gsub(",+",","):gsub("%s+"," ")
            end
            version = logt[value].hash
            version = tags and (version .. " " .. tags) or version
        end)
    end
    parameter.action("pull", function()
        parameter.clear()
        message.warning = "This will replace the current code on device " ..
            "with the version from the remote repository"
        parameter.watch("message.warning")
        parameter.action("Yes, do it", function()
            if b.status.working then return end
            local ref = version ~= "latest" and version or "master"
            ref = ref:match("[^%s]+")
            b.status:startAnimation()
            pull(b.project, ref, function(res)
                b.status:stopAnimation()
                local t = serializer.decode(res)
                if t.clone_status == 0 and t.checkout_status == 0 then
                    for name, content in pairs(t.tabs) do
                        saveProjectTab(b.project.name .. ":" .. name, content)
                    end
                    local c = listProjectTabs(b.project.name)
                    for i,name in ipairs(c) do
                        if not t.tabs[name] and name ~= "Main" then
                            print("deleting tab " .. name)
                            saveProjectTab(b.project.name .. ":" .. name, nil)
                        end
                    end
                    -- Info.plist
                    if t.plist then
                        saveProjectPlist(b.project.name, t.plist)
                    end
                    -- Icon
                    if t.icon then
                        saveProjectFile(b.project.name, "Icon.png",
                            hexDecode(t.icon))
                    end
                    b.status.c = (ref == "master" or ref == "HEAD" 
                        or (logt and logt[1].hash == ref))
                        and Status.green or Status.purple
                else
                    b.status.c = Status.grey
                end
            end, function(err)
                print("PROBLEM: " .. tostring(err))
                b.status:stopAnimation()
                b.status.c = Status.grey
            end)
            version = nil
            setParameters()
        end)
        parameter.action("No, keep what i have", function()
            version = nil
            setParameters()
        end)
    end)
    if not showHistory then
        parameter.action("recent history", function()
            getLog(b)
        end)
    end
    parameter.action("cancel", function()
        version = nil
        b.status:stopAnimation()
        setParameters()
    end)
end

function showDiff(t)
    if t.diff then
        --alert(t.diff, "diff")
        diffW:show(t.diff)
    end
end

function iterProjects()
    local t = {}
    for _,k in ipairs(listLocalData()) do
        local name = string.match(k, "project[.]([^.]+)[.]remote")
        if name then
            t[name] = {name = name, remote = readLocalData(k)}
        end
    end
    local names = {}
    for k,_ in pairs(t) do
        names[#names + 1] = k
    end
    table.sort(names, function(a,b)
        return tostring(a):lower() < tostring(b):lower()
    end)
    local j = 0
    return function()
        j = j + 1
        local k = names[j]
        return k and j,t[k] or nil
    end
end

-- This function gets called once every frame
function draw()
    -- This sets a dark background color 
    background(50, 50, 55, 255)

    -- Do your drawing here
    if panel then
        panel:draw()
    end
    
    font("GillSans")
    fontSize(22)
    if not sx then
        sx,sy = textSize(STATUS)
    end
    fill(141, 143, 157, 255)
    textMode(CORNER)
    textAlign(RIGHT)
    text(STATUS, WIDTH-sx-12, 12)
    
    diffW:draw()
end

function touched(touch)
    if diffW:touched(touch) then
        -- diff window is modal
        return
    end
    panel:touched(touch)
end

function prepData(project)
    local data = {
        remote = project.remote,
        ["plist"] = readProjectPlist(project.name),
        ["icon"] = hexEncode(readProjectFile(project.name, "Icon.png")),
    }
    local t = listProjectTabs(project.name)
    for i,name in ipairs(t) do
        data.tabs = data.tabs or {}
        data.tabs[name] = readProjectTab(project.name .. ":" .. name)
    end
    return data
end

function postTo(path, body, cb, eb)
    if not user then
        if eb then eb("No identity") end
        return
    end
    http.request(
        apiBase .. path,
        function(data, status, headers)
            local maxlen = 200
            if #data > maxlen then
                print("response:", data:sub(1,maxlen), "...")
            else
                print("response:", data)
            end
            if cb then cb(data) end
        end,
        function(error)
            if eb then eb(error) end
        end,
        { method = "POST", data = body,
          headers = { Authorization = "CodeaScmUser " .. user}}
    )
end

function status(project, get_diff, f, e)
    local data = prepData(project)
    if not data.tabs then
        return e("no tabs found for project: " .. project.name)
    end
    data.get_diff = get_diff
    data.pk = private_key
    postTo("/status.lua", serializer.encode(data), f, e)
end

function push(project, comment, f, e)
    local data = prepData(project)
    if not data.tabs then
        return e("no tabs found for project: " .. project.name)
    end
    data.comment = comment ~= "" and comment or nil
    data.pk = private_key
    postTo("/checkpoint.lua", serializer.encode(data), f, e)
end
        
function pull(project, ref, f, e)
    if #listProjectTabs(project.name) < 1 then
        return e(string.format("project '%s' does not seem to exist. " ..
            "Please create it first.", project.name))
    end
    local data = {remote = project.remote, ref = ref, pk = private_key}
    postTo("/pull.lua", serializer.encode(data), f, e)
end

function log(project, f, e)
    local n = tonumber(readLocalData("logLength")) or 5
    local data = {remote = project.remote, n = n, pk = private_key}
    postTo("/log.lua", serializer.encode(data), f, e)
end
