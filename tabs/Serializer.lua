Serializer = class()

local function assign(obj, name_spec, value)
    if not name_spec or name_spec == ""  then return obj end
    local o = obj
    for w, d in name_spec:gmatch('([%w_]+)(%.?)') do
        w = tonumber(w) or w
        if d == "." then
            o[w] = o[w] or {}
            o = o[w]
        else
            o[w] = value
            break
        end
    end
    return obj
end

local function decode(data)
    local t = {}
    local pos, len = 1, #data
    local in_string, line
    local line_count = 0
    while pos < len do
        local s, e = data:find('\n', pos)
        if not s then
            line = data:sub(pos)
        else
            line = data:sub(pos, s-1)
        end
        line_count = line_count + 1
        local name, value = line:match('([%w_.]+)%s*=%s*([^\r]*)')
        if name and value then
            local marker = value:match('%[([=]*)%[')
            if marker then
                -- it's a string
                local sb, eb = data:find(string.format('[[]%s[[]', marker), pos)
                local sn, en = data:find(string.format(']%s]', marker), pos)
                if sn then
                    assign(t, name, data:sub(eb + 1, sn - 1))
                    pos = en + 1
                else
                    return nil, string.format("unterminated string in line: #%s", line_count)
                end
            else
                local v = tonumber(value)
                if v then
                    -- it's a number
                    assign(t, name, v)
                else
                    -- must be a boolean or nil
                    v = string.lower(value:match('(%w+)') or '')
                    if v == 'true' or v == 'false' then
                        assign(t, name, v == 'true')
                    end
                end
                pos = e and (e + 1) or len
            end
        else
            pos = e and (e + 1) or len
        end
    end
    return t
end

local function _encode(t, r, parent)
    for k,v in pairs(t) do
        k = tostring(k)
        local name = parent and (parent .. "." .. k) or k
        if type(v) == 'table' then
            _encode(v, r, name)
        elseif type(v) == 'string' then
            local marker = v:match('%[([=]*)%[')
            marker = marker or v:match(']([=]*)]')
            marker = marker and (marker .. "=") or ""
            r[#r + 1] = string.format('%s = [%s[%s]%s]', name, marker, v, marker)
        else
            r[#r + 1] = string.format('%s = %s', name, tostring(v))
        end
    end
end

local function encode(t)
    local r = {}
    _encode(t, r)
    r[#r + 1] = ""
    return table.concat(r, '\r\n')
end
    
function Serializer:init()
    self.engine = {
        decode = decode,
        encode = encode,
    }
end
